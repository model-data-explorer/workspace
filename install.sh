
[ ! $PYTHON ] && PYTHON=python

$PYTHON -m venv venv

source venv/bin/activate

pip install -U pip

SCRIPTPATH="$( cd -- "$(dirname "$0")" >/dev/null 2>&1 ; pwd -P )"

# install submodules
pip install -r ${SCRIPTPATH}/docs/workspace/requirements.txt
pip install -r ${SCRIPTPATH}/docs/main/requirements.txt
pip install -r ${SCRIPTPATH}/docs/prototype/requirements.txt
pip install -r ${SCRIPTPATH}/python/django-psql-dag-test/requirements.txt
pip install -e ${SCRIPTPATH}/python/django-helmholtz-aai[dev]

# for code extensions for python
pip install black isort mypy pylint autopep8 flake8 rstcheck sphinx-autobuild
