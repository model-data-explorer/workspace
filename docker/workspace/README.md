# Model Data Explorer Workspace

Docker-based Workspace for developing the Model Data Explorer


Please checkout the docs to learn how to use and install this workspace:

https://model-data-explorer.readthedocs.io/projects/workspace
