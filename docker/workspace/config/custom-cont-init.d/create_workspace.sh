#!/bin/bash
# populate the workspace
#
# This script clones the workspace repository at https://gitlab.hzdr.de/model-data-explorer/workspace.git
# and installs it at /config/workspace, including the python virtual
# environment. Additionally, it starts some static file servers to host the
# documentation. They are accessible with the browser at
#
# /proxy/9001 https://gitlab.hzdr.de/model-data-explorer/model-data-explorer.pages.hzdr.de
# /proxy/9002 https://gitlab.hzdr.de/model-data-explorer/prototype
#
# NOTE: We do not overwrite any existing folder here

set -e
cd /config/workspace

if [[ ! -d .git ]]; then
    cp -r /config/workspace_src/. .
    git submodule update --init
    git remote set-url origin https://gitlab.hzdr.de/model-data-explorer/workspace.git
fi

if [[ ! -d venv ]]; then
    echo "**** setting up python ****"

    PYTHON=python3.8 ./install.sh
else
    echo "**** python already setup. skipping ****"
fi

# build and serve the docs
source venv/bin/activate

# main repo
[ ! -d docs/main/build/html ] && sphinx-build docs/main/source docs/main/build/html
nohup python -m http.server --directory docs/main/build/html 9001 &

# prototype repo
[ ! -d docs/prototype/build/html ] && sphinx-build docs/prototype/source docs/prototype/build/html
nohup python -m http.server --directory docs/prototype/build/html 9002 &

# workspace repo
[ ! -d docs/workspace/build/html ] && sphinx-build docs/workspace/source docs/workspace/build/html
nohup python -m http.server --directory docs/workspace/build/html 9003 &

# django-helmholtz-aai docs
[ ! -d python/django-helmholtz-aai/build/html ] && make -C python/django-helmholtz-aai/docs/ html
nohup python -m http.server --directory python/django-helmholtz-aai/docs/_build/html 9004 &

chown -R abc: /config/workspace
