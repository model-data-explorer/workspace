#!/bin/bash
# Switch git to a different user with different GPG Key

set -e

function Help() {
    cat << EOF
Switch git to a different user with different GPG Key

This command takes a full name and an email as argument switches the global
git user to use this.

Call syntax::

    git switch-user "Full Name <full.name@example.com>"


Syntax: git switch-user [-h] UID

mandatory arguments:
UID    The UID of the GPG Key of the user
Email       The email of the user

options:
h     Print this Help.
EOF
}

function trim() {
    local STRING="$*"
    # remove leading spaces
    STRING="${STRING#"${STRING%%[![:space:]]*}"}"
    # remove trailing spaces
    STRING="${STRING%"${STRING##*[![:space:]]}"}"
    printf '%s' "$STRING"
}


while getopts ":h" option; do
   case $option in
      h) # display Help
         Help
         exit;;
     \?) # incorrect option
         echo "Error: Invalid option"
         exit 1;;
   esac
done

FPR=""
GPG_UID="${1}"

if [[ "${GPG_UID}" == "" ]]; then
    >&2 echo "Please specify the UID of the GPG Key to use"
    echo ""
    Help
    exit 1
fi

FPR=`git get-user-fpr "${GPG_UID}"`
if [[ "$FPR" == "" ]]; then
    echo "Could not find GPG key for ${GPG_UID}"
else
    EMAIL=`echo "${GPG_UID}" | grep -Po '<\K[^>]+(?=>)'`
    FULLNAME=`echo "${GPG_UID}" | grep -Po  '.*?(?=\<)'`
    FULLNAME="`trim ${FULLNAME}`"
    git config --global user.name "${FULLNAME}"
    git config --global user.email "${EMAIL}"
    git config --global user.signingkey ${FPR}

    echo "Switching user to"
    git show-current-user
fi