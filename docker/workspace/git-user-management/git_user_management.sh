#!/bin/bash

function __complete_git_users() {
    local PUID SPLITTED LINE
    local start="${COMP_WORDS[$COMP_CWORD]}"

    # remove leading and trailing quotes
    if [[ $start =~ ^\" || $start =~ ^\' ]]; then
        start="${start/#\"/}"
        start="${start/#\'/}"
        start="${start/ /\\ }"
    fi

    COMPREPLY=()
    while IFS= read -r GPG_PUID; do
        if [[ "${GPG_PUID// /___}" =~ ^${start// /___} ]]; then
            COMPREPLY+=( "\"${GPG_PUID}\"" )
        fi
    done < <(git-show-users)
}

_git_switch_user() {
    __complete_git_users git-switch-user "${@:2}"
}

_git_get_user_fpr() {
    __complete_git_users git-switch-user "${@:2}"
}

complete -F __complete_git_users git-switch-user
complete -F __complete_git_users git-get-user-fpr
