#!/bin/bash

set -e

# insert user password
sed -i "\|^</tomcat-users>|i <role rolename=\"admin-gui\"/>" /usr/local/tomcat/conf/tomcat-users.xml
sed -i "\|^</tomcat-users>|i <role rolename=\"manager-gui\"/>" /usr/local/tomcat/conf/tomcat-users.xml
sed -i "\|^</tomcat-users>|i <role rolename=\"manager-script\"/>" /usr/local/tomcat/conf/tomcat-users.xml
if [[ "${MANAGER_USER}" != "" ]] && [[ "${MANAGER_PASSWORD}" != "" ]]; then
    DIGEST_OUTPUT=`/usr/local/tomcat/bin/digest.sh -a "sha-512" ${MANAGER_PASSWORD}`
    IFS=":" read -r -a SPLITTED <<< "${DIGEST_OUTPUT}"
    DIGESTED_PW=${SPLITTED[1]}
    sed -i "\|^</tomcat-users>|i <user username=\"${MANAGER_USER}\" password=\"${DIGESTED_PW}\" roles=\"admin-gui,manager-gui,manager-script\"/>" /usr/local/tomcat/conf/tomcat-users.xml
fi

# add hosts for manager app
if [[ "${ALLOWED_MANAGER_HOSTS}" != "" ]]; then
    IFS="," read -r -a SPLITTED_HOSTS <<< "${ALLOWED_MANAGER_HOSTS}"
    for ALLOWED_HOST in "${SPLITTED_HOSTS[@]}"; do
        for HOST_IP in $(getent hosts ${ALLOWED_HOST} | awk '{ print $1 }'); do
            ALLOWED_HOST_IPS=${ALLOWED_HOST_IPS}${HOST_IP}'|'
        done
    done
    sed -i "s/allow=\"127.*\"/allow=\"${ALLOWED_HOST_IPS::-1}\"/g" /usr/local/tomcat/webapps/manager/META-INF/context.xml
fi

/entrypoint.sh "$@"
