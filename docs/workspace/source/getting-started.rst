.. _getting-started:

Getting started
===============

We recommend that you create a GPG key for your workspace by running
:git-cmd:`git add-user "Your Name" your@email.com <git add-user>`. Alternatively
you can drag-and-drop a secret key to this browser window and import it via
:git-cmd:`git import-user path/to/your/key <git import-user>`. Alternatively,
if you do not intend to sign your commits, you can also skip this step and
:ref:`disable the user management <disable-user-management>`.

Now you can get more into the folder structure and work on the project that you
are interested in.

Folder structure
----------------

-  the workspace is located at ``/config/workspace``. This folder is
   also mounted locally at ``./workspace`` such that you can access the
   files from the filesystem of your localhost
-  thredds configuration and data files are at
   ``/config/workspace/thredds``
-  the modules for the model data explorer are installed at start from
   the `workspace <https://gitlab.hzdr.de/model-data-explorer/workspace>`__
   repository at ``/config/workspace/`` when starting the container.

   .. list-table::
       :header-rows: 1

       * - Project
         - Path
         - Workspace URL
         - Proxied URL

       * - `Main documentation`_
         - ``/config/workspace/docs/main``
         - http://127.0.0.1:8443/?folder=/config/workspace/docs/main
         - http://127.0.0.1:8443/proxy/9001/
       * - `Prototype documentation`_
         - ``/config/workspace/docs/prototype``
         - http://127.0.0.1:8443/?folder=/config/workspace/docs/prototype
         - http://127.0.0.1:8443/proxy/9002/
       * - `Workspace documentation`_
         - ``/config/workspace/docs/workspace``
         - http://127.0.0.1:8443/?folder=/config/workspace/docs/workspace
         - http://127.0.0.1:8443/proxy/9003/
       * - django-helmholtz-aai_ package
         - ``/config/workspace/python/django-helmholtz-aai``
         - http://127.0.0.1:8443/?folder=/config/workspace/python/django-helmholtz-aai
         - http://127.0.0.1:8443/proxy/9004/

   .. _Main documentation: https://model-data-explorer.readthedocs.io/
   .. _Prototype documentation: https://model-data-explorer.readthedocs.io/projects/prototype
   .. _Workspace documentation: https://model-data-explorer.readthedocs.io/projects/workspace
   .. _django-helmholtz-aai: https://django-helmholtz-aai.readthedocs.io
-  the GPG based user management is mounted from your local
   ``users_config`` folder at ``/config/users_config`` folder. The
   ``$HOME/.gnupg`` and ``$HOME/.git-coauthors`` link to the folder/file
   in that directory.