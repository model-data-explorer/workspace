.. _install-server:

Deployment on a server
======================
At Hereon, we deploy this workspace at on of our servers in the internal
LAN that has docker installed. This server is managed by an
nginx-proxy_ that maps the code-server container to a
local domain, and contols the access via basic authentification.

1. Setup a container with nginx (if you do not already have one):

   .. code:: bash

      docker run -d --name nginx-proxy --restart always -p 80:80 -p 443:443 \
        -v /opt/hcdc/htpasswd:/etc/nginx/htpasswd \
        -v /opt/hcdc/certs:/etc/nginx/certs \
        -v /var/run/docker.sock:/tmp/docker.sock:ro \
        nginxproxy/nginx-proxy

   The paths to ``/opt/hcdc/`` should of course be adapted to your
   needs.
2. Store your SSL certificates at ``/opt/hcdc/certs`` (if you have one)
3. Copy the template environment files to the root of your repository:
   ``cp templates/*.env .``
4. If you want to run your server at http(s)://mde-dev.myserver.local,
   you should add the following entry to the ``code-server.env`` file:
   ``VIRTUAL_HOST=mde-dev.myserver.local``.
5. If you are running the server behind a proxy, you should change the
   corresponding lines ``HTTP_PROXY``, ``HTTPS_PROXY``, etc. in
   ``code-server.env`` **and** ``proxy_settings.env``.
6. If you want to password protect your server, you have two choices:

   1. set the ``PASSWORD`` or ``HASHED_PASSWORD`` environment variable
      (see https://github.com/linuxserver/docker-code-server#usage)
   2. if you are using an
      nginx-proxy_ setup
      (see step 1 above), you can create a file named
      ``mde-dev.myserver.local`` at ``/opt/hcdc/htpasswd`` (or whatever
      you mounted in the ``nginx-proxy`` container at
      ``/etc/nginx/htpasswd``) via:

      .. code:: bash

         echo "your-login:$(openssl passwd -apr1)" > /opt/hcdc/htpasswd/mde-dev.myserver.local

      Type in the password that you want to choose. Once you setup the
      containers, you will be asked for your login (``your-login``) and
      your password.
7. Clone the workspace repository via ``git clone gitlab.hzdr.de/model-data-explorer/workspace.git``
8. Now you are good to go to build and deploy the workspace on your
   server:

   .. code:: bash

      docker-compose up --build

   and access it at http://mde-dev.myserver.local.
9. Connect the ``nginx_proxy`` container to the ``frontend`` network. If
   you cloned this repository into a folder named ``docker-workspace``,
   your network is named ``docker-workspace_frontend`` and you can
   connect the ``nginx_proxy`` via
   ``docker network connect docker-workspace_frontend nginx-proxy``. See
   ``docker network ls`` for a list of available networks and choose an
   appropriate one and restart it via ``docker restart nginx-proxy``.


Now you're ready to :ref:`get started with this workspace <getting-started>`.

.. _nginx-proxy: https://github.com/nginx-proxy/nginx-proxy
