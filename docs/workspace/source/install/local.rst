.. _install-local:

Local installation
==================

To install this workspace locally, all you have to do is to run

1. install docker_ or podman_
2. install docker-compose_
3. Clone the repository.

   .. warning::

       On Windows, make sure to run ``git config --global core.autocrlf false``
       prior to cloning the repository

   ``git clone https://gitlab.hzdr.de/model-data-explorer/workspace.git``
4. Change into the directory: ``cd workspace``
5. copy the template files to the root of your repository:
   ``cp docker/workspace/templates/*.env .``
6. run ``docker-compose up --build``

You will then be able to open a code server in your browser by visiting
http://localhost:8443.

Now you're ready to :ref:`get started with this workspace <getting-started>`.

.. _docker: https://docs.docker.com/engine/install/
.. _podman: https://podman.io/getting-started/installation
.. _docker-compose: https://docs.docker.com/compose/install/