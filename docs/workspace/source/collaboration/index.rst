.. _collaboration:

Collaboration in the workspace
==============================

The docker workspace is made for collaboration. You can
:ref:`deploy this workspace on a server <install-server>` and use the
management tools that we describe here to track contributions within the
git commit system.

The *user management* is based on a GPG system and provides commands like
:git-cmd:`git add-user`, :git-cmd:`git import-user` and
:git-cmd:`git switch-user` commands. See :ref:`user-management` for more
information.

Another tool that we use to foster collaboration is the
:ref:`git mob <git-mob>` command. With this tool, you can mark commits as
collaborative efforts of multiple users.

.. toctree::

    users
    git-mob
