.. _user-management:

Git user management
===================

The docker workspace implements a user management to track who is creating a
commit. This is based on GPG keys, a common framework to verify a git commit.

.. warning::

    The user management that we describe here does not protect your
    workspace from external access. For this, you should either use a password
    protection or setup an reverse proxy (see :ref:`install-server`).

The basic idea is that every user with access to a workspace creates (or
imports) a GPG key. When the user then commits changes, he or she will sign it
with his personal GPG key.

This procedure is based on a well-known mechanism to verify git commits and
is supported by gitlab_ and github_. We elaborate this workflow by adding a
few shortcut commands listed below.

.. note::

    The GPG based user management is mounted from your local ``users_config``
    folder at ``/config/users_config`` folder. So if you , if you delete the
    container, your generated GPG keys will still be available via
    ``GNUPGHOME=./users_config/.gnupg gpg --list-keys``


.. _disable-user-management:

Disable user management
-----------------------

This is the default and recommended workflow. If you do not intend to sign
your commits, you can disable it. For this, you need to remove the
``git.enableCommitSigning`` from settings.json_, and run
``git config --global commit.gpgsign false`` when your workspace is up and
running.

.. _settings.json: https://gitlab.hzdr.de/model-data-explorer/docker-workspace/-/blob/main/config/data/User/settings.json

.. _git mob: https://github.com/findmypast-oss/git-mob

Command Overview
----------------

.. list-table::

    * - :git-cmd:`git add-user`
      - Add a new user (GPG key)
    * - :git-cmd:`git import-user`
      - Import a user from an existing GPG key
    * - :git-cmd:`git switch-user`
      - Switch to a different user (GPG key)

Furthermore, we added a few convenience commands to get information about the
users:

.. list-table::

    * - :git-cmd:`git show-current-user`
      - Show information about the current user
    * - :git-cmd:`git show-users`
      - Show the available user IDs
    * - :git-cmd:`git get-user-fpr`
      - Show the fingerprint of the GPG key of a user
    * - :git-cmd:`git add-coauthors`
      - Add all GPG UIDs as git-coauthors

.. _gitlab: https://gitlab.hzdr.de/help/user/project/repository/gpg_signed_commits/index.md
.. _github: https://docs.github.com/en/github/authenticating-to-github/managing-commit-signature-verification


User management commands
------------------------

.. git-cmd:: git add-user

    .. program:: git-add-user

    Convenience command to create a GPG key and add a git co-author

    This command takes a full name and an email as argument and creates a
    GPG key (the user is asked for a passphrase). At the end, this function
    adds the newly created user to ~/.git-coauthors and opens the public
    key using code-server to import it at gitlab.

    .. note::

        This command sets the generated user as the current git user
        (via ``git config --global user.name`` and
        ``git config --global user.email``)

    Syntax::

        git add-user [-h] FullName Email

    .. card:: Positional arguments

        .. option:: FullName

            The full name of the user

        .. option:: Email

            The email of the user

    .. card:: Optional arguments

        .. option:: -h

            Print the help

    .. admonition:: Example

        .. code-block:: bash

            $ git add-user "Full Name" full.name@example.com

        .. dropdown:: Output

            .. code-block::

                About to create a key for:
                    "Full Name <full.name@example.com>"

                Continue? (Y/n) Y
                We need to generate a lot of random bytes. It is a good idea to perform
                some other action (type on the keyboard, move the mouse, utilize the
                disks) during the prime generation; this gives the random number
                generator a better chance to gain enough entropy.
                We need to generate a lot of random bytes. It is a good idea to perform
                some other action (type on the keyboard, move the mouse, utilize the
                disks) during the prime generation; this gives the random number
                generator a better chance to gain enough entropy.
                gpg: key 97614C2CC63BC7DC marked as ultimately trusted
                gpg: revocation certificate stored as '/config/.gnupg/openpgp-revocs.d/A8BB6ADF12C373E7C7A816C897614C2CC63BC7DC.rev'
                public and secret key created and signed.

                pub   rsa3072 2021-08-22 [SC] [expires: 2023-08-22]
                    A8BB6ADF12C373E7C7A816C897614C2CC63BC7DC
                uid                      Full Name <full.name@example.com>
                sub   rsa3072 2021-08-22 [E]

                gpg: checking the trustdb
                gpg: marginals needed: 3  completes needed: 1  trust model: pgp
                gpg: depth: 0  valid:   1  signed:   0  trust: 0-, 0q, 0n, 0m, 0f, 1u
                gpg: next trustdb check due at 2023-08-22

                Full Name has been added to the .git-coauthors file

                Switching user to
                Name: Full Name
                Email: full.name@example.com
                GPG Fingerprint: A8BB6ADF12C373E7C7A816C897614C2CC63BC7DC

                You are almost done! We created a file at /tmp/gpg.jOW9Jrmbj/A8BB6ADF12C373E7C7A816C897614C2CC63BC7DC.asc for you. You see it in the
                editor above. Just paste the public key at https://gitlab.hzdr.de/-/profile/gpg_keys
                and you're done.


.. git-cmd:: git import-user

    .. program:: git-import-user

    Convenience command to import a GPG key and add a git co-author

    This command takes a path to a private GPG key and imports it (the user is
    asked for a passphrase). At the end, this function adds the newly created user
    to ``~/.git-coauthors``.

    .. note::

        This command sets the generated user as the current git user
        (via ``git config --global user.name`` and
        ``git config --global user.email``)

    Syntax::

        git import-user [-h] PathToKey

    .. card:: Positional arguments

        .. option:: PathToKey

            The path to the secret key that you want to import

    .. card:: Optional arguments

        .. option:: -h

            Print this Help.

    .. admonition:: Example

        .. code-block:: bash

            $ gpg --export-secret-keys --armor E63AD84182A1E4D5EC47136A17DAB65E023A1933 > fullname.asc

        .. code-block:: bash

            $ git import-user fullname.asc

        .. dropdown:: Output

            .. code-block::

                gpg: key 17DAB65E023A1933: public key "Full Name <full.name@example.com>" imported
                gpg: key 17DAB65E023A1933: secret key imported
                gpg: Total number processed: 1
                gpg:               imported: 1
                gpg:       secret keys read: 1
                gpg:   secret keys imported: 1

                Full Name has been added to the .git-coauthors file

                Switching user to
                Name: Full Name
                Email: full.name@example.com
                GPG Fingerprint: E63AD84182A1E4D5EC47136A17DAB65E023A1933


.. git-cmd:: git switch-user

    .. program:: git-switch-user

    Switch git to a different user with different GPG Key

    This command takes a full name and an email as argument switches the global
    git user to use this via ``git config --global user.name`` and
    ``git config --global user.email``.


    Syntax::

        git switch-user [-h] UID

    .. card:: Positional arguments

        .. option:: UID

            The UID of the GPG Key of the user
        .. option:: Email

            The email of the user

    .. card:: Optional arguments

        .. option:: -h

            Print this Help.

    .. admonition:: Example

        .. code-block:: bash

            $ git switch-user "Full Name <full.name@example.com>"
            Switching user to
            Name: Full Name
            Email: full.name@example.com
            GPG Fingerprint: 26BFABC2CFD18DB82933FB8F7BC5E91EDD9DDEA1


User information commands
-------------------------

.. git-cmd:: git show-current-user

    .. program:: git-show-current-user

    Show the current global user

    Syntax::

        git show-current-user [-h]

    .. card:: Optional arguments

        .. option:: -h

            Print this help.

    .. admonition:: Example

        .. code-block:: bash

            $ git show-current-user
            Name: Full Name
            Email: full.name@example.com
            GPG Fingerprint: 26BFABC2CFD18DB82933FB8F7BC5E91EDD9DDEA1


.. git-cmd:: git show-users

    .. program:: git-show-users

    Show all GPG UIDs

    This command looks up the GPG UIDs and displays them.

    Syntax::

        git show-users [-h]

    .. card:: Optional arguments:

        .. option:: -h

            Print this Help.

    .. admonition:: Example

        .. code-block:: bash

            $ git show-users
            Full Name <full.name@example.com>
            Another Name <another.name@example.com>


.. git-cmd:: git get-user-fpr

    .. program:: git-get-user-fpr

    Get the fingerprint of a UID

    This function takes one argument, the UID as ``"Full Name <email>"`` and
    returns the corresponding fingerprint (if there is one).

    Syntax::

        git get-user-fpr [-h] UID

    .. card:: Positional arguments

        .. option:: UIDorFile

            The UID of the GPG Key or the path to am exported GPG key

    .. card:: Optional arguments

        .. option:: -h

            Print this Help.

    .. admonition:: Example

        .. code-block:: bash

            $ git get-user-fpr "Full Name <full.name@example.com>"
            E63AD84182A1E4D5EC47136A17DAB65E023A1933

Other management commands
-------------------------

.. git-cmd:: git add-coauthors

    .. program:: git-add-coauthors

    Add all GPG UIDs as git-coauthors

    This command looks up the GPG keys and adds one entry for each key to the
    ``~/.git-coauthors`` file.

    .. note::

        You usually do not have to invoke this command manually as both, the
        :git-cmd:`git add-user` and :git-cmd:`git import-user` command add the
        user to this file.

    Syntax::

        git add-coauthors [-h]

    .. card:: Optional arguments

        .. option:: -h

            Print this Help.

    .. admonition:: Example

        .. code-block:: bash

            $ git add-coauthors
            Full Name has been added to the .git-coauthors file
            Another Name has been added to the .git-coauthors file
