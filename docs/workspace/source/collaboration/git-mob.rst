.. _git-mob:

Using ``git mob`` to manage coauthors
=====================================
This the docker workspace installs the `git-mob npm package`_ to easy handle
collaborative commits. It basically works like this:

1. The ``/config/.git-coauthors`` file contains information about the
   potential coauthors. Each coauthor has a full name and an email. You
   can list all coauthors via `git mob -l`. E.g.

   .. code-block:: bash

       $ git mob -l
       fn Full Name full.name@example.com
       an Another Name another.name@example.com

2. If you collaborate with another user, you should add this person as
   a coauthor, e.g. via ``git mob fn``

   This adds a statement like
   ``Co-authored-by: Full Name <full.name@example.com>`` to every commit
   message. Gitlab and github are both able to understand this format
   and will contribute the commit to both, the one who does the commit and
   the coauthor.

See the docs on the `git-mob npm package`_ for more information.

.. note::

    Commands like :git-cmd:`git add-user` and :git-cmd:`git import-user`
    automatically add coauthors to the ``~/.git-coauthors`` file.

.. note::

    The ``.git-coauthors`` file is mounted from your local ``users_config``
    folder at ``/config/users_config`` folder. So if you , if you delete the
    container, your coauthors will still be available from
    ``./users_config/.git-coauthors``

.. _git-mob npm package: https://www.npmjs.com/package/git-mob