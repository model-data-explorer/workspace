.. _landing-page:

Welcome to the Model Data Explorer Development Workspace!
=========================================================


.. toctree::
    :caption: Contents
    :maxdepth: 1

    install/index
    getting-started
    collaboration/index

.. toctree::
   :maxdepth: 4
   :hidden:
   :caption: Resources

   Main documentation <https://model-data-explorer.readthedocs.io>
   Prototype development <https://model-data-explorer.readthedocs.io/projects/prototype/>

ToDos
-----

.. todolist::

Disclaimer
----------
This documentation is published under a CC-BY 4.0 license. Please see the
*LICENSE* file in the source code repository for details.

Copyright (c) 2021, Helmholtz-Zentrum hereon GmbH


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
