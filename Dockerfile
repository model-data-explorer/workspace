# syntax=docker/dockerfile:1
FROM ghcr.io/linuxserver/code-server

ENV PYTHONUNBUFFERED=1

# install necessary libraries using apt-get
RUN apt-get update && \
  apt-get install -y \
    bash-completion vim iputils-ping \
    graphviz \
    libpq-dev build-essential \
    postgresql postgresql-contrib \
    man-db manpages-posix \
    python3-venv python3-dev python3-psycopg2 \
    python3.8-venv python3.8-dev && \
  rm -rf /var/lib/apt/lists/*

# install git-mob
RUN npm i -g git-mob

COPY docker/workspace/config /config
COPY docker/workspace/git-user-management /config/git-user-management
RUN make -C /config/git-user-management install

ADD . /config/workspace_src

ENV PATH="/config/bin:${PATH}"

EXPOSE 8443
